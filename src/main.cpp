#include <cstdlib>
#include <drogon/drogon.h>
#include <fmt/color.h>
#include <fmt/format.h>

auto main() -> int {
    auto text_style{fmt::fg(fmt::color::lime) | fmt::emphasis::italic};
    fmt::print(text_style, "Hello, {}!\n", "World");
    drogon::app().setLogLevel(trantor::Logger::kWarn).addListener("0.0.0.0", 8080).setThreadNum(4).run();
    return EXIT_SUCCESS;
}
