cmake_minimum_required(VERSION 3.28)

project(dependencies)
include(ExternalProject)

set(CMAKE_STAGING_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/staging)
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_STAGING_PREFIX}")
list(APPEND CMAKE_FIND_ROOT_PATH "${CMAKE_STAGING_PREFIX}")

# https://stackoverflow.com/questions/51121295/cmake-find-module-to-distinguish-shared-or-static-library
set(CMAKE_FIND_LIBRARY_SUFFIXES "${CMAKE_STATIC_LIBRARY_SUFFIX}")
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

if(NOT DEFINED TARGET_HOST)
  execute_process(
    COMMAND ${CMAKE_C_COMPILER} -dumpmachine
    OUTPUT_VARIABLE TARGET_HOST
    OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

if(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  set(TARGET_OS "unknown")
endif()

list(
  APPEND
  CMAKE_CACHE_ARGS
  "-DCMAKE_TOOLCHAIN_FILE:FILEPATH=${CMAKE_TOOLCHAIN_FILE}"
  "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}"
  "-DCMAKE_PREFIX_PATH:STRING=${CMAKE_PREFIX_PATH}"
  "-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_STAGING_PREFIX:PATH=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_FIND_ROOT_PATH:STRING=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_FIND_LIBRARY_SUFFIXES:STRING=${CMAKE_FIND_LIBRARY_SUFFIXES}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE:STRING=${CMAKE_FIND_ROOT_PATH_MODE_PACKAGE}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE:STRING=${CMAKE_FIND_ROOT_PATH_MODE_INCLUDE}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY:STRING=${CMAKE_FIND_ROOT_PATH_MODE_LIBRARY}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM:STRING=${CMAKE_FIND_ROOT_PATH_MODE_PROGRAM}"
  "-DBUILD_SHARED_LIBS:BOOL=OFF")

externalproject_add(
  zlib
  URL https://github.com/madler/zlib/releases/download/v1.3.1/zlib-1.3.1.tar.gz
  URL_HASH SHA256=9a93b2b7dfdac77ceba5a558a580e74667dd6fede4585b91eefb60f03b72df23
  CMAKE_CACHE_ARGS ${CMAKE_CACHE_ARGS} "-DZLIB_BUILD_EXAMPLES:BOOL=OFF")

externalproject_add(
  jsoncpp
  URL https://github.com/open-source-parsers/jsoncpp/archive/refs/tags/1.9.5.zip
  URL_HASH SHA256=a074e1b38083484e8e07789fd683599d19da8bb960959c83751cd0284bdf2043
  CMAKE_CACHE_ARGS ${CMAKE_CACHE_ARGS} "-DBUILD_STATIC_LIBS:BOOL=ON" "-DJSONCPP_WITH_TESTS:BOOL=OFF"
                   "-DJSONCPP_WITH_POST_BUILD_UNITTEST:BOOL=OFF")

externalproject_add(
  cares
  URL https://github.com/c-ares/c-ares/releases/download/cares-1_27_0/c-ares-1.27.0.tar.gz
  URL_HASH SHA256=0a72be66959955c43e2af2fbd03418e82a2bd5464604ec9a62147e37aceb420b
  CMAKE_CACHE_ARGS ${CMAKE_CACHE_ARGS} "-DCARES_STATIC:BOOL=ON" "-DCARES_STATIC_PIC:BOOL=ON"
                   "-DCARES_BUILD_TOOLS:BOOL=OFF")

externalproject_add(
  brotli
  URL https://github.com/google/brotli/archive/refs/tags/v1.1.0.zip
  URL_HASH SHA256=9d7ec775e67cdb3d0328f63f314b381d57f0f985499bbf2e55b15138a3621b19
  CMAKE_CACHE_ARGS ${CMAKE_CACHE_ARGS} "-DBROTLI_BUILD_TOOLS:BOOL=OFF")

externalproject_add(
  libuuid
  URL https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.40/util-linux-2.40.tar.xz
  URL_HASH SHA256=d57a626081f9ead02fa44c63a6af162ec19c58f53e993f206ab7c3a6641c2cd7
  CONFIGURE_COMMAND
    "${CMAKE_CURRENT_BINARY_DIR}/libuuid-prefix/src/libuuid/configure" "--prefix=${CMAKE_STAGING_PREFIX}"
    "--host=${TARGET_HOST}" "--disable-all-programs" "--enable-shared=no" "--enable-static" "--enable-libuuid"
    "PKG_CONFIG_PATH=${CMAKE_STAGING_PREFIX}"
    "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
  BUILD_COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
  INSTALL_COMMAND make install)

externalproject_add(
  sqlite
  URL "https://github.com/sqlite/sqlite/archive/refs/tags/version-3.45.2.tar.gz"
  URL_HASH "SHA256=bcfe8994d500c50fb525dde6e8114c8f34534727b572a6da810653acfc1f8aaf"
  CONFIGURE_COMMAND
    "${CMAKE_CURRENT_BINARY_DIR}/sqlite-prefix/src/sqlite/configure" #
    "--prefix=${CMAKE_STAGING_PREFIX}" #
    "--host=${TARGET_ARCH}-${TARGET_VENDOR}-${TARGET_OS}" #
    "--enable-all" #
    "--disable-tcl" #
    "--enable-shared=no" #
    "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
    "CFLAGS=-DSQLITE_ENABLE_COLUMN_METADATA $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
  BUILD_COMMAND "make" "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
  INSTALL_COMMAND "make" "install")

list(
  APPEND
  OPENSSL_CONFIG_CMD
  "${CMAKE_CURRENT_BINARY_DIR}/openssl-prefix/src/openssl/Configure" #
  "--prefix=${CMAKE_STAGING_PREFIX}" #
  "--openssldir=${CMAKE_STAGING_PREFIX}/etc/ssl" #
  "no-asm" #
  "no-err" #
  "no-apps" #
  "no-shared" #
  "no-module" #
  "no-weak-ssl-ciphers" #
  "-DPEDANTIC" #
)
if("${TARGET_VENDOR}" STREQUAL "w64")
  list(APPEND OPENSSL_CONFIG_CMD "mingw64")
elseif("${TARGET_OS}" STREQUAL "android")
  if("${TARGET_ARCH}" STREQUAL "aarch64")
    list(APPEND OPENSSL_CONFIG_CMD "android-arm64")
  elseif("${TARGET_ARCH}" STREQUAL "arm")
    list(APPEND OPENSSL_CONFIG_CMD "android-arm")
  endif()
else()
  list(APPEND OPENSSL_CONFIG_CMD "${TARGET_OS}-${TARGET_ARCH}")
endif()

externalproject_add(
  openssl
  URL "https://github.com/openssl/openssl/releases/download/openssl-3.2.1/openssl-3.2.1.tar.gz"
  URL_HASH "SHA256=83c7329fe52c850677d75e5d0b0ca245309b97e8ecbcfdc1dfdc4ab9fac35b39"
  DOWNLOAD_EXTRACT_TIMESTAMP TRUE
  CONFIGURE_COMMAND "${OPENSSL_CONFIG_CMD}"
  BUILD_COMMAND "make" "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
                "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
  INSTALL_COMMAND "make" "install_sw")

externalproject_add(
  botan
  URL https://botan.randombit.net/releases/Botan-3.3.0.tar.xz
  URL_HASH SHA256=368f11f426f1205aedb9e9e32368a16535dc11bd60351066e6f6664ec36b85b9
  CONFIGURE_COMMAND
    "${CMAKE_CURRENT_BINARY_DIR}/botan-prefix/src/botan/configure.py" "--prefix=${CMAKE_STAGING_PREFIX}"
    "--os=${TARGET_OS}" "--cpu=${TARGET_ARCH}"
    "--cc=$<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>>"
    "--cc-bin=${CMAKE_CXX_COMPILER}" "--ar-command=${CMAKE_AR}" "--no-install-python-module" "--disable-shared-library"
    "--without-sphinx" "--without-rst2man" "--disable-cc-tests"
    "--cc-abi-flags=$<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>:--target=${CMAKE_CXX_COMPILER_TARGET}>"
  BUILD_COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
  INSTALL_COMMAND make install)

externalproject_add(
  trantor
  GIT_REPOSITORY "https://github.com/an-tao/trantor.git"
  GIT_TAG "v1.5.21"
  GIT_SHALLOW TRUE
  DEPENDS botan cares
  CMAKE_CACHE_ARGS ${CL_ARGS} "-DCMAKE_CXX_STANDARD:STRING=17" "-DTRANTOR_USE_TLS:STRING=botan")

externalproject_add(
  drogon
  GIT_REPOSITORY https://github.com/drogonframework/drogon.git
  GIT_TAG v1.9.7
  GIT_SHALLOW TRUE
  DEPENDS libuuid
          jsoncpp
          zlib
          brotli
          trantor
          sqlite
          botan
  CMAKE_CACHE_ARGS
    ${CMAKE_CACHE_ARGS}
    "-DCMAKE_CXX_STANDARD:STRING=20"
    "-DUSE_STATIC_LIBS_ONLY:BOOL=ON"
    "-DBUILD_CTL:BOOL=OFF"
    "-DBUILD_EXAMPLES:BOOL=OFF"
    "-DCOZ_PROFILING:BOOL=OFF"
    "-DBUILD_DOC:BOOL=OFF"
    "-DBUILD_YAML_CONFIG:BOOL=OFF"
    "-DUSE_SUBMODULE:BOOL=OFF"
    "-DBUILD_POSTGRESQL:BOOL=OFF"
    "-DBUILD_MYSQL:BOOL=OFF"
    "-DBUILD_REDIS:BOOL=OFF")

externalproject_add(
  pugixml
  GIT_REPOSITORY "https://github.com/zeux/pugixml.git"
  GIT_TAG "v1.14"
  GIT_SHALLOW TRUE
  CMAKE_CACHE_ARGS ${CMAKE_CACHE_ARGS})

set(ICU_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/icu-prefix)
externalproject_add(
  icu
  URL https://github.com/unicode-org/icu/releases/download/release-75-1/icu4c-75_1-src.tgz
  URL_HASH SHA256=cb968df3e4d2e87e8b11c49a5d01c787bd13b9545280fc6642f826527618caef
  CONFIGURE_COMMAND
    "${ICU_PREFIX}/src/icu/source/configure" "--prefix=${CMAKE_STAGING_PREFIX}" "--host=${TARGET_HOST}"
    "--enable-static" "--enable-shared=no" "--disable-dyload" "--enable-tools=no" "--enable-tests=no"
    "--enable-samples=no" "--with-cross-build=${ICU_PREFIX}/src/icu/source" "PKG_CONFIG_PATH=${CMAKE_STAGING_PREFIX}"
    "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
    "CXX=${CMAKE_CXX_COMPILER} $<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>:--target=${CMAKE_CXX_COMPILER_TARGET}>"
  BUILD_COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
  INSTALL_COMMAND make install)
externalproject_add_step(
  icu build_host
  DEPENDEES patch
  DEPENDERS configure
  WORKING_DIRECTORY ${ICU_PREFIX}/src/icu/source
  COMMAND ./configure
  COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}")

externalproject_add(
  boost
  URL https://github.com/boostorg/boost/releases/download/boost-1.85.0/boost-1.85.0-cmake.tar.xz
  URL_HASH SHA256=0a9cc56ceae46986f5f4d43fe0311d90cf6d2fa9028258a95cab49ffdacf92ad
  BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/boost-prefix/src/boost"
  CONFIGURE_COMMAND "B2_DONT_EMBED_MANIFEST=on" #
                    "${CMAKE_CURRENT_BINARY_DIR}/boost-prefix/src/boost/bootstrap.sh" "--prefix=${CMAKE_STAGING_PREFIX}"
  DEPENDS "openssl"
  BUILD_COMMAND
    "./b2" "--user-config=user-config.jam" #
    "--with-headers" #
    "--with-log" #
    "--with-url" #
    "--with-system" #
    "--with-program_options" #
    "link=static" #
    "runtime-link=static" #
    "threading=multi" #
    "variant=release" #
    "--prefix=${CMAKE_STAGING_PREFIX}" #
    "-j${CMAKE_BUILD_PARALLEL_LEVEL}" #
    "$<$<STREQUAL:${TARGET_OS},mingw32>:threadapi=pthread>"
    "target-os=$<IF:$<STREQUAL:${TARGET_OS},mingw32>,windows,${TARGET_OS}>"
    "toolset=$<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>>-${TARGET_ARCH}"
    "install"
  INSTALL_COMMAND "")

externalproject_add_step(
  boost configure_compiler
  DEPENDEES configure
  DEPENDERS build
  WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/boost-prefix/src/boost"
  COMMAND
    "echo"
    "using $<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>> : ${TARGET_ARCH} : ${CMAKE_CXX_COMPILER} $<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>: --target=${CMAKE_CXX_COMPILER_TARGET}> : $<SEMICOLON>"
    ">" "user-config.jam")

# Requires previous compilation for the host and installation at the QT_HOST_PATH
externalproject_add(
  qt6
  URL https://download.qt.io/official_releases/qt/6.7/6.7.0/single/qt-everywhere-src-6.7.0.tar.xz
  URL_HASH SHA256=bf5089912364f99cf9baf6c109de76a3172eec6267f148c69800575c47f90087
  DEPENDS openssl
  CMAKE_CACHE_ARGS
    ${CMAKE_CACHE_ARGS}
    "-DBUILD_WITH_PCH=ON"
    "-DQT_HOST_PATH:PATH=$ENV{SDK_ROOT}/qt6"
    "-DQT_BUILD_SUBMODULES:STRING=qtbase$<SEMICOLON>qtdeclarative"
    "-DOPENSSL_USE_STATIC_LIBS:BOOL=ON"
    "-DOPENSSL_ROOT_DIR:PATH=${CMAKE_STAGING_PREFIX}"
    "-DINPUT_opengl:STRING=$<IF:$<BOOL:${ANDROID}>,es2,opengl>"
    "$<$<BOOL:${ANDROID}>:-DQT_ANDROID_JAVAC_SOURCE=17>"
    "$<$<BOOL:${ANDROID}>:-DQT_ANDROID_JAVAC_TARGET=17>"
    "$<$<BOOL:${ANDROID}>:-DANDROID_NDK_ROOT:PATH=$ENV{NDK}>"
    "$<$<BOOL:${ANDROID}>:-DANDROID_SDK_ROOT:PATH=$ENV{SDK_ROOT}/android>")
