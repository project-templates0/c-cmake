cmake_minimum_required(VERSION 3.28)

configure_file(Dependencies.cmake dependencies/CMakeLists.txt COPYONLY)
configure_file(CMakePresets.json dependencies/CMakePresets.json COPYONLY)

get_filename_component(BUILD_TYPE_DIR ${CMAKE_CURRENT_BINARY_DIR} NAME)
set(CMAKE_STAGING_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/dependencies/${BUILD_TYPE_DIR}/staging)
list(APPEND CMAKE_PREFIX_PATH "${CMAKE_STAGING_PREFIX}")
list(APPEND CMAKE_FIND_ROOT_PATH "${CMAKE_STAGING_PREFIX}")

# https://stackoverflow.com/questions/51121295/cmake-find-module-to-distinguish-shared-or-static-library
set(CMAKE_FIND_LIBRARY_SUFFIXES "${CMAKE_STATIC_LIBRARY_SUFFIX}")
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

if(NOT DEFINED TARGET_HOST)
  execute_process(
    COMMAND ${CMAKE_C_COMPILER} -dumpmachine
    OUTPUT_VARIABLE TARGET_HOST
    OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

if(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  set(TARGET_OS "unknown")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --preset ${CMAKE_PRESET_NAME} #
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dependencies COMMAND_ERROR_IS_FATAL ANY)
execute_process(COMMAND ${CMAKE_COMMAND} --build --preset ${CMAKE_PRESET_NAME}
                WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/dependencies COMMAND_ERROR_IS_FATAL ANY)
